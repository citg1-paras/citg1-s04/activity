package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;

	public void init() throws ServletException {
		System.out.println("********************************");
		System.out.println("UserServlet has been initialized");
		System.out.println("********************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		res.sendRedirect("index.html");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		String firstName = req.getParameter("firstname");
		String lastName = req.getParameter("lastName");
		String email = req.getParameter("email");
		String contact = req.getParameter("contact");
		
//		First name - System Properties
		System.setProperty("firstName", firstName);
		
//		Last name - HttpSession
        HttpSession session = req.getSession();
        session.setAttribute("lastName", lastName);
        
//      Email - ServletContext setAttribute
        ServletContext srvContext = getServletContext();
        srvContext.setAttribute("email", email);
        
//      Contact number - URL Rewriting via sendRedirectmethod  
        res.sendRedirect("details?contact=" + contact);

	}
	
	public void destroy() {
		System.out.println("****************************");
		System.out.println("UserServlet has been destroy");
		System.out.println("****************************");
	}
}
