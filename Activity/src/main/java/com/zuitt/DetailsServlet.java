package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758676644158652494L;
	
	public void init() throws ServletException {
		System.out.println("***********************************");
		System.out.println("DetailsServlet has been initialized");
		System.out.println("***********************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession session = req.getSession();
        ServletConfig config = getServletConfig();
        
//		First name - System Properties
        String firstName = System.getProperty("firstName");
//		Last name - HttpSession
        String lastName = session.getAttribute("lastName").toString();
//      Email - ServletContext setAttribute
        String email = config.getServletContext().getAttribute("email").toString();
//      Contact number - URL Rewriting via sendRedirectmethod 
        String contact = req.getParameter("contact");
        

        
        
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        out.println(
    			"<h1>Welcome to Phonebook</h1>" +
    			"<p>First Name: " + firstName + "</p>" +
    			"<p>Last Name: " + lastName + "</p>" +
    			"<p>Email:: " + email + "</p>" +
    			"<p>Contact: " + contact + "</p>" 
    				);
	}
	
	public void destroy() {
		System.out.println("*******************************");
		System.out.println("DetailsServlet has been destroy");
		System.out.println("*******************************");
	}

}
